<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    // /**
    //  * @return Book[] Returns an array of Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getListByPage(int $page, $limit = 100)
    {

        return $this->createQueryBuilder('p')
            ->setFirstResult($page* $limit)
            ->setMaxResults($limit)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();

    }

    public function getListByQueryAndPage(string $query, int $page, bool $is_isbn, $limit = 100)
    {


        $qb = $this->createQueryBuilder('p');

        if ($is_isbn) {
            $qb
                ->where($qb->expr()->orX(
                    $qb->expr()->like('p.isbn', '?1')
                ))
                ->setParameter('1', '%' .substr($query, 1, strlen($query)) . '%');

        } else {
            $qb
                ->where($qb->expr()->orX(
                    $qb->expr()->like('p.title', '?1')
                ))
                ->setParameter('1', '%' . addcslashes($query, '%_') . '%');

        }

        $qb->setFirstResult(($page) * $limit)
            ->setMaxResults(($page + 1) * $limit)
            ->orderBy('p.id', 'DESC');


        $list = $qb->getQuery()->getResult();

        return $list;

    }


    public function countRows(){

        $qb = $this->createQueryBuilder('p');

        $qb->select($qb->expr()->count('p.id'));

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count;
    }


    public function removeAllBooks()
    {
        $connection = $this->getEntityManager()->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeStatement($platform->getTruncateTableSQL('book'));
    }


}
