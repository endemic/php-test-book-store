<?php


namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class APIController
 * @package App\Controller
 *
 * @Route("/api/book")
 */
class APIController extends AbstractController
{



    /**
     * @Route("/pagination/{page}", name="api_pagination", methods={"GET"})
     * @param int $page
     * @param BookRepository $repository
     * @return JsonResponse
     */
    public function pagination(int $page, BookRepository $repository): JsonResponse
    {

        $list = $repository->getListByPage($page);

        $result = array_map(function ($book) {
            return $this->transform($book);
        }, $list);

        return new JsonResponse(['list' => $result], 200);
    }


    /**
     * @Route("/search", name="api_search", methods={"POST"})
     * @param Request $request
     * @param BookRepository $repository
     * @return JsonResponse
     */
    public function search(Request $request, BookRepository $repository): JsonResponse
    {

        $query = $request->request->get('query', '');
        $page = $request->request->get('page', 0);
        $is_search_isbn = substr($query, 0, 1) === '#';

        $list = $repository->getListByQueryAndPage($query, $page, $is_search_isbn);

        $result = array_map(function ($book) {
            return $this->transform($book);
        }, $list);

        return new JsonResponse(['list' => $result], 200);
    }


    /**
     * @param Book $book
     * @return array
     */
    private function transform(Book $book)
    {
        return [
            'id' => $book->getId(),
            'title' => $book->getTitle(),
            'isbn' => $book->getIsbn(),
            'image' => $book->getImage(),
            'description' => $book->getDescription()
        ];
    }
}