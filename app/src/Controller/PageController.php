<?php


namespace App\Controller;

use App\Message\ImportXML;
use App\Repository\BookRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\File;

class PageController extends AbstractController
{

    /**
     * @Route("/", name="app_index")
     */
    public function index(): Response
    {

        return $this->render('page/index.html.twig', []);
    }

    /**
     * @Route("/admin", name="app_admin")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param BookRepository $bookRepository
     * @return Response
     */
    public function admin(Request $request, MessageBusInterface $bus, BookRepository $bookRepository): Response
    {

        $form = $this->createFormBuilder(['message' => 'Upload XML file'])
            ->add('file', FileType::class, [
                'label' => '',
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '128M',
                        'mimeTypes' => [
                            'text/xml',
                            'application/xml',
                            'application/x-zip-compressed',
                            'application/x-zip',
                            'application/zip',
                            'application/application/octet-stream',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid XML or ZIP file',
                    ])
                ],
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'save'],
            ])
            ->getForm();


        $form->handleRequest($request);

        $is_working = count(scandir($this->getParameter('xml_directory'))) > 3;

        if ($form->isSubmitted() && $form->isValid() && !$is_working) {

            /** @var UploadedFile $file */
            $file = $form->get('file')->getData();

            if ($file) {

                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                $translate = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $filename);

                $store = $translate . '-' . uniqid() . '.' . $file->guessExtension();

                $file->move($this->getParameter('xml_directory'), $store);

                $bus->dispatch(new ImportXML($store));

                return $this->render('page/success.html.twig', [
                    'form' => $form->createView(),
                ]);
            }

            return $this->render('page/error.html.twig', [
                'form' => $form->createView()
            ]);
        }

        return $this->render('page/admin.html.twig', [
            'form' => $form->createView(),
            'is_working' => $is_working,
            'counter'=>$is_working?$bookRepository->countRows():0
        ]);

    }


}