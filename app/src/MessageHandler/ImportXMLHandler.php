<?php


namespace App\MessageHandler;


use App\Entity\Book;
use App\Message\ImportXML;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use Intervention\Image\ImageManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

use Exception;

class ImportXMLHandler extends AbstractController implements MessageHandlerInterface
{

    const LIMIT_ORM = 30;

    const LIMIT_POOL = 5;

    /** @var EntityManagerInterface */
    private $em;

    /** @var string */
    private $image_folder;

    /** @var string */
    private $image_url;

    /** @var string */
    private $folder_pattern;

    /** @var Client */
    private $client;

    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var array */
    private $images;

    public function __construct(EntityManagerInterface $entityManager, string $targetDirectory, string $width, string $height)
    {

        $this->em = $entityManager;

        $this->folder_pattern = $targetDirectory;

        $this->height = (int)$height;
        $this->width = (int)$width;

        $this->images = [];
        $this->client = new Client();
    }


    public function __invoke(ImportXML $importXML)
    {

        print 'Import data from ' . $importXML->getFilename() . PHP_EOL;

        $this->em->getRepository(Book::class)->removeAllBooks();

        $this->image_folder = sprintf("%s/%s/%s/%s", $this->folder_pattern, date('Y'), date('m'), date('d'));
        $this->image_url = sprintf("/%s/%s/%s/%s", 'storage', date('Y'), date('m'), date('d'));

        $this->removeFolder($this->image_folder);
        $this->createFolder($this->image_folder);

        $this->import($importXML);

        print 'Import complete for ' . $importXML->getFilename() . PHP_EOL;

        $this->clearFolder($this->getParameter('xml_directory'));

        return 1;

    }


    /**
     * @param $folder
     * @return bool
     */
    private function removeFolder($folder): bool
    {

        if (!file_exists($folder)) {
            return true;
        }

        if (!is_dir($folder)) {
            return unlink($folder);
        }

        foreach (scandir($folder) as $item) {
            if ($item == '.' || $item == '..' || $item == '.gitignore') {
                continue;
            }

            if (!$this->removeFolder($folder . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }
        return rmdir($folder);

    }

    private function clearFolder($folder): void
    {

        foreach (scandir($folder) as $item) {
            if ($item == '.' || $item == '..' || $item == '.gitignore') {
                continue;
            }

            unlink($folder . '/' . $item);
        }
    }


    /**
     * @param $folder
     */
    private function createFolder($folder)
    {

        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }

    }


    /**
     * @param ImportXML $importXML
     * @throws Exception
     */
    public function import(ImportXML $importXML)
    {

        // parse XML
        $reader = new \XMLReader();
        $doc = new \DOMDocument();

        $full_path = sprintf("%s/%s", $this->getParameter('xml_directory'), $importXML->getFilename());

        # Open if uploading archive
        if (mime_content_type($full_path) === 'application/zip') {
            $full_path = sprintf("zip://%s#test.xml", $full_path);
        }

        // Open file
        $reader->open($full_path);
        $counter = 0;
        $isbn_list = [];

        // Read
        while ($reader->read()) {

            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->name == 'book') {

                $node = simplexml_import_dom($doc->importNode($reader->expand(), true));

                if (!in_array($reader->getAttribute('isbn'), $isbn_list)) {

                    array_push($isbn_list, $reader->getAttribute('isbn'));

                    $random_name = sprintf("%s.jpg", bin2hex(random_bytes(10)));

                    $book = new Book();

                    $book
                        ->setIsbn($reader->getAttribute('isbn'))
                        ->setImage(sprintf("%s/%s", $this->image_url, $random_name))
                        ->setTitle($reader->getAttribute('title'))
                        ->setDescription($node->description);

                    array_push($this->images, ['source' => ((array)$node->image)[0], 'local' => sprintf("%s/%s", $this->image_folder, $random_name)]);

                    $this->em->persist($book);
                    $counter++;
                }
            }

            if ($counter > self::LIMIT_ORM) {
                $counter = $this->flush();
            }
        }

        $counter = $this->flush();
    }

    private function flush(){

        $this->PoolGuzzle($this->images);
        $this->resizeImages($this->images);
        $this->images = [];

        $this->em->flush();
        $this->em->clear();

        return 0;
    }



    private function PoolGuzzle($images)
    {

        $client = new Client();

        $requests_func = function ($targets) use ($client) {
            foreach ($targets as $target) {
                yield $target => function () use ($client, $target) {
                    return $client->getAsync($target['source'], ['sink' => $target['local']]
                    );
                };
            }
        };

        $pool = new Pool($client, $requests_func($images), [
            'concurrency' => self::LIMIT_POOL,
            'fulfilled' => function (Response $response, $index) {
            },
            'rejected' => function (Exception $reason, $index) {
            },
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();


        // Force the pool of requests to complete
        $promise->wait();
    }

    private function resizeImages($images)
    {

        foreach ($images as $image) {
            $this->resizeSingleImage($image['local']);
        }

    }

    /**
     * @param $full_path
     */
    private function resizeSingleImage($full_path): void
    {

        $manager = new ImageManager(array('driver' => 'gd'));

        # Create image
        $image = $manager->make($full_path);

        # resize image instance to fit
        $image->fit($this->width, $this->height, function ($constraint) {
            $constraint->upsize();
        });

        # save image
        $image->save($full_path);

    }

}