import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import '../assets/styles/app.css'
import 'bootstrap/dist/css/bootstrap.css'

Vue.use(Vuex);

Vue.config.devtools = process.env.NODE_ENV === 'development';

// app specific
import router from "./vue/router";
import store from "./vue/store";
import App from "./vue/src/App";

Vue.use(VueRouter);

// router check
router.beforeEach((to, from, next) => {
    return next();
});

new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
});
