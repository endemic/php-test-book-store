import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

let state = {

};

const actions = {

};

const mutations = {

};

let store = new Vuex.Store({
    state,
    actions,
    mutations
});

export default store;