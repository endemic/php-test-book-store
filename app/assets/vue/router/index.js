import Router from 'vue-router'

import Index from "../src/Page/Index";

// components

let router = new Router({
    mode: 'history',
    linkExactActiveClass: 'selected',
    routes: [{
        path: '/',
        name: 'homepage',
        component: Index
    },
    ]
});

export default router;