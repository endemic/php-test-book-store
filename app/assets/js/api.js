import axios from 'axios'
import Book from "./transform/book";

const routePagination = '/api/book/pagination/'
const routeSearch = '/api/book/search'

export default class API {

    static fetchBookByPagination(page) {

        return axios.get(routePagination + page.toString()).then(response => {
            if (response.status === 200) {

                let books = response.data.list;

                let result = [];
                books.forEach(item => {
                    result.push(Book.book(item))
                })

                return result;
            }
            throw Error('Wring Response code');
        })
    }

    static fetchBookBySearchPagination(query, page) {

        let data = new FormData();
        data.append('query', query)
        data.append('page', page)

        return axios.post(routeSearch, data).then(response => {
            if (response.status === 200) {

                let books = response.data.list;

                let result = [];
                books.forEach(item => {
                    result.push(Book.book(item))
                })

                return result;
            }
        })
    }

}