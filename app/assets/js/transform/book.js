export default class Book {

    transform(book) {
        return Book.book(book)
    }

    static book(item) {
        return {
            'id': item['id'],
            'isbn': item['isbn'],
            'title': item['title'],
            'image': item['image'],
            'description': item['description'],
        }
    }
}