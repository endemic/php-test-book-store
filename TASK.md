#### Description:

Create a simple application calls Books Library. 


#### Details

1. It should contain data from the provided XML file (500000 records).

2. Each book must have a name, image (book cover) with images size "200x400" and must be cropped.

3. You need to have a form where we can upload XML file, and it should proceed after an upload the test file. Use queue jobs to import this file in background.

    Here the fields for the books table:
    
    ```
    Title: string, not null, 190 limit
    Image: string, null
    ISBN: string, not null, unique
    Description: longtext
    ```

#### Parser:
We have XML with books data -> import it. 
1. You need to write a simple parser which will parse all the items from the XML and will create the records in the database. 
2. Write a parser which will import all data to the database. It must check that the book doesn't exist in the database based on ISBN (titles can be duplicated), download image from URL and store it to the *public/storage/{year}/{month}/{date}/{unique filename}.jpg*. 
3. If book already exists it must skip this book and continue work with other items. If image not set for the book it must store book and skip the image saving process.

#### Frontend:
1. Use any popular CSS framework (bootstrap, talwind or any other) and show full list of the books which you have in the database with pagination (Show 100 books on one page). 
2. Pagination should work, user should able to search data by ISBN or title via one field.
 
```
We don't care about frontend part and we need just to see that you know how to use vue.js.
```

#### Deploy
After completing the test task share your code for review to the bitbucket or github with installment instructions in README.md file. We will install it locally and will check how it works.

#### Notes:
- Make sure that you have separated business logic. Don't use controllers or models for the business logic.
- Not needed to comment the code. Make sure that names of the methods answer on question what they are do.
- PHP 7.3
- Symfony 4 or 5