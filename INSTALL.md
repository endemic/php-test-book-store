#### Install book-store

! Do not forget create SWAP-space on VPS

1. Clone repository
    
    ```
    git clone https://gitlab.com/*****
    ```
2. Copy .env-dist to .env and change/set in .env (where docker-compose.yml)
     
    ```text
   ADMIN_PASSWORD=   - password to admin zone
   APP_SECRET=   -symfony secret string
   
   MYSQL_ROOT_PASSWORD=  - root password to mysql
   MYSQL_PASSWORD=   - book_store_user password, need to work with DB
   
    ```
3. Set logs folder
    ```
    sudo chmod -R 777 ./logs
    ```   

4. Check that you don't use ports by another application
    ```
   SERVER_PORT_NGINX=8040
   SERVER_PORT_PHPADMIN=8100
   ``` 

5. Start docker container 
    
    In cloned folder 
    ```
    docker-compose up -d
    ```

6. Setup in the docker container 
    ```
   docker-compose exec app bash
   
   COMPOSER_MEMORY_LIMIT=-1 composer install

   php bin/console make:migration
   php bin/console doctrine:migrations:migrate
   
   yarn install
   yarn encore prod
   ``` 
   
7. If you use *docker-compose-dev.yml* (for developing), you will able to check data in MySQL (by phpmyadmin), and you will have to manually start messenger:compose

    ```
    docker-compose exec app bash
    php bin/console messenger:consume async
    ```    
   
**End. Open app by http://{ip}:{SERVER_PORT_NGINX}**

Admin panel -> http://{ip}:{SERVER_PORT_NGINX}/admin

    ```
    Login: admin
    Password: {ADMIN_PASSWORD} - from .env
    ```